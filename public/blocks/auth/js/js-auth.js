(
    function () {
        // Заводим новый блок
        var authBlock = {};

        // Регистрация
        WindCore.blockRegister(
            // Новый блок объединяется с существующим "WindBlock"
            $.extend(
                authBlock,
                WindBlock, {
                    // Устанавливаем родительский блок
                    getSelector: function () {
                        return '.js-auth';
                    },
                    // Возвращает массив объектов с которыми работаем
                    getBindings: function () {
                        // Сохраняем текущий контекст
                        var self = this;
                        return [
                            // Скрываем аутентификацию при клике на оверлэй
                            [
                                'click',
                                '.js-auth-overlay',
                                function (event) {
                                    event.preventDefault();
                                    $('.js-auth').addClass('-hide');
                                }
                            ]
                        ];
                    },
                    // Вызывается при загрузке странице
                    initialize: function ($target) {
                    }
                }
            )
        );
    }()
);